import * as types from './mutation-types'
import Vue from 'vue'

export const fetchPost = ({ commit }, slug) => {
  Vue.http.get('post/' + slug).then((response) => {
    commit(types.SET_POST, response.data)
  }, (response) => {
    commit(types.SET_POST, '')
  })
}
