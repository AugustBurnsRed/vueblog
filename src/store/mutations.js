// import { set } from 'vue'
import * as types from './mutation-types'

export default {
  [types.SET_POST] (state, post) {
    state.post = post
  }
}
