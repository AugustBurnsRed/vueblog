import Blog from './components/Blog/Blog.vue'

export default [
  {
    path: '/',
    component: { template: '<h1>Accueil</h1>' }},
  {
    path: '/test',
    component: { template: '<h1>2e page</h1>' }},
  {
    path: '/blogue',
    name: 'blog',
    component: Blog
  },
  {
    path: '/blogue/:id/:slug?',
    name: 'blog-post',
    component: Blog
  }
]
