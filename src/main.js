import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

// install router
Vue.use(VueRouter)
Vue.use(VueResource)
Vue.http.options.root = 'http://localhost'
// Vue.http.headers.common['Authorization'] = 'Basic YXBpOnBhc3N3b3Jk'

import App from './App.vue'
import routes from './routes'

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  scrollBehavior: () => ({ y: 0 }),
  routes
})

new Vue({
  router,
  ...App
}).$mount('#app')
